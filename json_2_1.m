path='C:\Users\student\Desktop\jsonlab';
addpath(path);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%dane z danepubliczne.imgw.pl
%katowice_1

url_1=('https://danepubliczne.imgw.pl/api/data/synop/id/12560/format/json');

data_1=loadjson(urlread(url_1));
disp(data_1); %writes all data from the link above 

%below prints the given data as characters, not numbers
fprintf('Pressure : %s\n', data_1.cisnienie);
fprintf('Temperature : %s\n', data_1.temperatura);
fprintf('Speed : %s\n', data_1.predkosc_wiatru);
fprintf('Humidity : %s\n', data_1.wilgotnosc_wzgledna);
fprintf('Hour : %s\n', data_1.godzina_pomiaru);


%change characters to numbers
Pressure_1=str2num(data_1.cisnienie)
Temperature_1=str2num(data_1.temperatura)
Speed_1=str2num(data_1.predkosc_wiatru)
Humidity_1=str2num(data_1.wilgotnosc_wzgledna)
Hour_1=str2num(data_1.godzina_pomiaru)

%table without title
array_1=[Pressure_1,Temperature_1,Speed_1,Humidity_1,Hour_1]



%from Jacek Pawlyta
% define file name to which will will write the data
fileName_1='datawheather.cvc';

% build two lines header of the table
header={'Pressure','Temperature','Speed','Humidity','Hour'};
headerUnits={''}

array_1=[Pressure_1,Temperature_1,Speed_1,Humidity_1,Hour_1];

% show on the screen how the data looks like
disp(' ');
disp(header);
disp(headerUnits);
disp(array_1);

% open datafile to write headers in it
% if file existed before it will be overwritten 'w+'

dataFileId=fopen(fileName_1,'w+');




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%czestochowa_2

url_2=('https://danepubliczne.imgw.pl/api/data/synop/id/12550/format/json');

data_2=loadjson(urlread(url_2));
disp(data_2);
fprintf('Pressure : %s\n', data_2.cisnienie);
fprintf('Temperature : %s\n', data_2.temperatura);
fprintf('Speed : %s\n', data_2.predkosc_wiatru);
fprintf('Humidity : %s\n', data_2.wilgotnosc_wzgledna);
fprintf('Hour : %s\n', data_2.godzina_pomiaru);

Pressure_2=str2num(data_2.cisnienie)
Temperature_2=str2num(data_2.temperatura)
Speed_2=str2num(data_2.predkosc_wiatru)
Humidity_2=str2num(data_2.wilgotnosc_wzgledna)
Hour_2=str2num(data_2.godzina_pomiaru)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Racib�rz_3
url_3=('https://danepubliczne.imgw.pl/api/data/synop/id/12540/format/json');

data_3=loadjson(urlread(url_3));
disp(data_3);
fprintf('Pressure : %s\n', data_3.cisnienie);
fprintf('Temperature : %s\n', data_3.temperatura);
fprintf('Speed : %s\n', data_3.predkosc_wiatru);
fprintf('Humidity : %s\n', data_3.wilgotnosc_wzgledna);
fprintf('Hour : %s\n', data_3.godzina_pomiaru);

Pressure_3=str2num(data_3.cisnienie)
Temperature_3=str2num(data_3.temperatura)
Speed_3=str2num(data_3.predkosc_wiatru)
Humidity_3=str2num(data_3.wilgotnosc_wzgledna)
Hour_3=str2num(data_3.godzina_pomiaru)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%dane z wttr.in
urw_1=('http://wttr.in/gliwice?m?M?0')
data_4=loadjson(urlread(urw_1));
disp(data_4);


fprintf('Pressure : %s\n', data_4.cisnienie);
fprintf('Temperature : %s\n', data_4.temperatura);
fprintf('Speed : %s\n', data_4.predkosc_wiatru);
fprintf('Humidity : %s\n', data_4.wilgotnosc_wzgledna);
fprintf('Hour : %s\n', data_4.godzina_pomiaru);

Pressure_4=str2num(data_4.cisnienie)
Temperature_4=str2num(data_4.temperatura)
Speed_4=str2num(data_4.predkosc_wiatru)
Humidity_4=str2num(data_4.wilgotnosc_wzgledna)
Hour_4=str2num(data_4.godzina_pomiaru)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
urw_2=('http://wttr.in/sosnowiec?m?M?0')
data_5=loadjson(urlread(urw_2));
disp(data_5);

fprintf('Pressure : %s\n', data_5.cisnienie);
fprintf('Temperature : %s\n', data_5.temperatura);
fprintf('Speed : %s\n', data_5.predkosc_wiatru);
fprintf('Humidity : %s\n', data_5.wilgotnosc_wzgledna);
fprintf('Hour : %s\n', data_5.godzina_pomiaru);

Pressure_5=str2num(data_5.cisnienie)
Temperature_5=str2num(data_5.temperatura)
Speed_5=str2num(data_5.predkosc_wiatru)
Humidity_5=str2num(data_5.wilgotnosc_wzgledna)
Hour_5=str2num(data_5.godzina_pomiaru)




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
urw_3=('http://wttr.in/warszawa?m?M?0')
data_6=loadjson(urlread(urw_3));
disp(data_6);

fprintf('Pressure : %s\n', data_6.cisnienie);
fprintf('Temperature : %s\n', data_6.temperatura);
fprintf('Speed : %s\n', data_6.predkosc_wiatru);
fprintf('Humidity : %s\n', data_6.wilgotnosc_wzgledna);
fprintf('Hour : %s\n', data_6.godzina_pomiaru);

Pressure_6=str2num(data_6.cisnienie)
Temperature_6=str2num(data_6.temperatura)
Speed_6=str2num(data_6.predkosc_wiatru)
Humidity_6=str2num(data_6.wilgotnosc_wzgledna)
Hour_6=str2num(data_6.godzina_pomiaru)
